const { Router } = require("express");
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage });

const { ciudadanoGet, ciudadanosPost } = require("../controllers/renap");
const { validarRequest } = require("../middlewares/validar_request");

const router = Router();

//BaseURL defined in server.js -> /api/renap

router.get("/:cui", ciudadanoGet);

router.post(
  "/upload-csv",
  [
    upload.single("file"),
    validarRequest
  ],
  ciudadanosPost
);

module.exports = router;
