const mongoose = require('mongoose');
const dbName = "renap";
const uri = `mongodb+srv://sa:sa@sa.4d9ky.mongodb.net/${dbName}?retryWrites=true&w=majority`;

const dbConnection = async () => {
  try {
    await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
    console.log('Microservicio renap conectado con la base de datos');
  } catch (error) {
    console.log(error);
    throw new Error('No fue posible conectarse a la base de datos');
  }
}

const dbDisconnect = async () => {
  try {
    await mongoose.connection.close();
  } catch (error) {
    console.log(error);
  }
}

module.exports = { dbConnection, dbDisconnect };