const request = require("supertest");
const Server = require('../models/server');

const server = new Server();
const app = server.app;

const cuiURL = '/api/renap';
const commonHeaders = {
  Authentication:
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdWkiOiIyNTE4OTA2NDMwMTAxIiwiaWF0IjoxNjE5MTE4ODg4LCJleHAiOjE2MjE3MTA4ODh9.FjXH3Qi2Hvvy2vA5nM6U78XY_uDgEuxoy6esbXWQN9o",
};

beforeAll(async (done) => {
  await server.connectWithDataBase();
  done();
});

describe("Prueba la ruta para consulta de ciudadanos en renap", () => {
  test("Debe retornar codigo 400 ya que no se le esta enviando token de autenticacion", async () => {
    const cui = 3035742910110;
    const response = await request(app)
      .get(`${cuiURL}/${cui}`);
    expect(response.statusCode).toBe(400);
  });
});

describe("Prueba la ruta para consulta de ciudadanos en renap", () => {
  test("Debe retornar codigo 200 y la información del usuario", async () => {
    const cui = 3035742910110;
    const response = await request(app)
      .get(`${cuiURL}/${cui}`)
      .set(commonHeaders);
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('cui');
  });
});

describe("Prueba la ruta para consulta de ciudadanos en renap", () => {
  test("Debe retornar codigo 200 aunque el usuario no este registrado en la base de datos y un mensaje de alerta", async () => {
    const cui = 2518;
    const response = await request(app)
      .get(`${cuiURL}/${cui}`)
      .set(commonHeaders);
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty("mensaje");
  });
});

afterAll(async (done) => {
  await server.disconnectDatabase();
  done();
});