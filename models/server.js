const express = require('express');
const cors = require('cors');
const { dbConnection, dbDisconnect } = require('../database/config');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT || 3000;
    this.baseUrl = "/api/renap";

    this.middelwares();
    this.routes();
  }

  middelwares() {
    //CORS
    this.app.use(cors());
    //Lectura y parseo del body
    this.app.use(express.json());
    //Directorio public
    // this.app.use(express.static("public"));
  }

  routes() {
    this.app.use(this.baseUrl, require("../routes/renap"));
  }

  //Conexión con la base de datos
  async connectWithDataBase() {
    await dbConnection();
  }

  async disconnectDatabase() {
    await dbDisconnect();
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("Servidor corriendo en puerto", this.port);
    });
  }
}

module.exports = Server;