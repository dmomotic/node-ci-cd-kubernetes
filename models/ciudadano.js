const { Schema, model } = require("mongoose");

const CiudadanoSchema = Schema({
  cui: {
    type: String,
    required: [true, "El atributo cui es obligatorio"],
    unique: true
  },
  nombres: {
    type: String,
    required: [true, "El atributo nombres es obligatorio"],
  },
  apellidos: {
    type: String,
    required: [true, "El atributo apellidos es obligatorio"],
  },
  fecha_nacimiento: {
    type: String,
    required: [true, "El atributo fecha_nacimiento es obligatorio"],
  },
  lugar_municipio: {
    type: String,
    required: [true, "El atributo lugar_municipio es obligatorio"],
  },
  lugar_departamento: {
    type: String,
    required: [true, "El atributo lugar_departamento es obligatorio"],
  },
  lugar_pais: {
    type: String,
    required: [true, "El atributo lugar_pais es obligatorio"],
  },
  nacionalidad: {
    type: String,
    required: [true, "El atributo nacionalidad es obligatorio"],
  },
  sexo: {
    type: String,
    required: [true, "El atributo sexo es obligatorio"],
    enum: ['M', 'F']
  },
  estado_civil: {
    type: Number,
    required: [true, "El atributo estado_civil es obligatorio"],
    enum: [0, 1]
  },
  servicio_militar: {
    type: Number,
    required: [true, "El atributo servicio_militar es obligatorio"],
    enum: [0, 1]
  },
  privado_libertad: {
    type: Number,
    required: [true, "El atributo privado_libertad es obligatorio"],
    enum: [0, 1]
  },
  foto: {
    type: String,
    required: [true, "El atributo foto es obligatorio"],
  },
  padron: {
    type: Number,
    required: [true, "El atributo padron es obligatorio"],
    unique: true
  },
});

CiudadanoSchema.methods.toJSON = function () {
  const { __v, _id, ...data } = this.toObject();
  return data;
}

module.exports = model('Ciudadano', CiudadanoSchema);