const { response, request } = require("express");
const csv = require("csvtojson");
const Ciudadano = require("../models/ciudadano");
const moment = require('moment');
const axios = require('axios').default;

const ciudadanosPost = async (req = request, res = response) => {
  try {
    //Validating token
    // const token = req.headers["authentication"];
    // if (!(await validToken(token))) {
    //   return res
    //     .status(400)
    //     .json({ status: 400, mensaje: "Token invalido" });
    // }

    const buffer = req.file["buffer"];
    const arrayOfObjects = await csv({ delimiter: [";", ","] }).fromString(
      buffer.toString()
    );

    //Validating size of array
    if (!arrayOfObjects || arrayOfObjects.length === 0) {
      return res
        .status(400)
        .json({ status: 400, mensaje: "Error en el archivo CSV" });
    }

    //Checking required keys
    const keysFromCsv = getKeysFromObject(arrayOfObjects[0]);
    const missedKeys = getMissedKeys(keysFromCsv);
    if (missedKeys.length > 0) {
      return res.status(400).json({
        status: 400,
        mensaje: `El archivo CSV debe incluir los siguientes campos: ${missedKeys}`,
      });
    }

    // Checking duplicated cui
    for (const item of arrayOfObjects) {
      const ciudadano = await Ciudadano.findOne({ cui: item.cui });
      if (ciudadano) {
        return res
          .status(400)
          .json({
            status: 400,
            mensaje: `Ya existe un ciudadano registrado con el cui: ${item.cui} en la base de datos de RENAP`,
          });
      }
    }

    const ciudadanos = arrayOfObjects.map((object) => new Ciudadano(object));

    //Checking Scheema model validations and date
    let errors = '';
    for (const ciudadano of ciudadanos) {
      errors += ciudadanoGetError(ciudadano);
      //Validating date
      if (!moment(ciudadano.fecha_nacimiento, "YYYY-MM-DD", true).isValid()) {
        errors += `fecha_nacimiento invalida: ${ciudadano.fecha_nacimiento} formato requerido YYYY-MM-DD \n`;
      }
    }
    if (errors.length > 0) {
      return res.status(400).json({
        status: 400,
        mensaje: errors,
      });
    }

    await Promise.all(
      ciudadanos.map(async (ciudadano) => {
        await ciudadano.save();
        return ciudadano;
      })
    );
    return res
      .status(200)
      .json({ status: 200, mensaje: "Archivo cargado a la base de datos" });
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      status: 400,
      mensaje: "No fue posible registrar la información en la base de datos",
    });
  }
};

const validToken = async (token) => {
  if (token == null) return false;
  try {
    const auxUrl = process.env.URL_JWT_PROD || '34.73.67.52';
    const url = `http://${auxUrl}/api/jwt`;
    const config = { headers: { "Authentication": token } };
    const response = await axios.get(url, config);
    if (response.status == 200 && response.data.isAuthentic == 1) {
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
}

const ciudadanoGetError = (ciudadano) => {
  const validation = ciudadano.validateSync();
  if (!!validation) {
    const errorObject = validation.errors;
    const field = Object.keys(errorObject)[0];
    return `Error en ${field}: ${errorObject[field]} \n`;
  }
  return '';
};

const ciudadanoGet = async (req = request, res = response) => {
  //Validating token
  const token = req.headers["authentication"];

  if (!(await validToken(token))) {
    return res.status(400).json({ status: 400, mensaje: "Token invalido" });
  }

  const { cui } = req.params;

  if (!cui) {
    return res.status(400).json({
      mensaje: "El parametro cui es obligatorio",
    });
  }

  const ciudadano = await Ciudadano.findOne({ cui });

  if (!ciudadano) {
    return res.json({
      mensaje: `No existe ningún ciudadano registradon con el cui: '${cui}' en la base de datos de RENAP`,
    });
  }

  res.json({
    status: 200,
    ...ciudadano.toObject(),
  });
};

const getKeysFromObject = (object) => {
  return Object.keys(object);
};

const getMissedKeys = (arrayOfKeys) => {
  const requiredKeys = [
    "cui",
    "nombres",
    "apellidos",
    "fecha_nacimiento",
    "lugar_municipio",
    "lugar_departamento",
    "lugar_pais",
    "nacionalidad",
    "sexo",
    "estado_civil",
    "servicio_militar",
    "privado_libertad",
    "foto",
    "padron",
  ];

  const difference = requiredKeys.filter((item) => !arrayOfKeys.includes(item));
  return difference;
};

module.exports = { ciudadanosPost, ciudadanoGet };
