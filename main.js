require("dotenv").config();
const Server = require("./models/server");

const server = new Server();

(async () => {
  await server.connectWithDataBase();
  server.listen();
  //testing with new cluster
})();

